def polinomio(lambda0):
    return lambda0**2 + lambda0 + 1


def normalizar(x, xmin, xmax, a, b):
    return a + (x - xmin)*(b - a)/(xmax - xmin)


def run():
    xmin = 3
    xmax = 421

    print("Eliga el intervalo en que desea normalizar los valores de la expresión lambda^2 + lambda + 1")
    print("Digite 1 para elegir el interválo: [-1, 1].")
    print("Digite 2 para elegir el interválo: [1, 10].")
    print("Digite 3 para elegir el interválo: [0.5, 1].")

    intervalo = int(input('----->'))

    if intervalo == 1:
        a = -1
        b = 1
    elif intervalo == 2:
        a = 1
        b = 10
    elif intervalo == 3:
        a = 0.5
        b = 1

    print(
        f"Cuando el intervalo de la normalizarión es [{a}, {b}], los valores son: ")

    for lambda0 in range(1, 21):
        x = polinomio(lambda0)
        xnor = normalizar(x, xmin, xmax, a, b)

        print(f'x = {x:>10.6f},    xnor = {xnor:>10.6f}')


if __name__ == '__main__':
    run()
