def seguiridad(saludar):
    password = input('Ingrese contraseña: ')

    def inner(nombre):
        if password == '123456':
            return saludar(nombre)
        else:
            print('Contraseña inválida')

    return inner


@seguiridad
def saludar(nombre):
    print(f'\nHola {nombre}\n')


def run():
    nombre = input('Ingrese un nombre para ser saludado: ')
    saludar(nombre)


if __name__ == '__main__':
    run()
