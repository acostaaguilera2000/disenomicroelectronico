'''
    Sí con << import plotext.plot as plx >>
    obtienen un error entonces reemplacenlo con:
    << import plotext as plx >>
'''

try:
    import plotext.plot as plx
except ModuleNotFoundError:
    import plotext as plx

from milibreria import cuadrado


def run():
    a = []
    b = []

    for i in range(-20, 21):
        a.append(i)
        b.append(cuadrado(i))

    plx.plot(a, b)
    plx.show()


if __name__ == '__main__':
    run()
