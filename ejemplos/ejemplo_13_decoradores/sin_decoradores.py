import numpy as np


def normalizar(x, xmax, xmin):
    return 2*(x - xmin)/(xmax - xmin) - 1


def ejemplo_sin_decorador():
    print('Ejemplo sin decorador:')

    x = np.array([x for x in range(1, 11)])

    xmax = 10
    xmin = 1

    xn = normalizar(x, xmax, xmin)

    for i in range(len(x)):
        print(f'{x[i]:5.2f}  {xn[i]:5.2f}')


def run():
    ejemplo_sin_decorador()


if __name__ == '__main__':
    run()
