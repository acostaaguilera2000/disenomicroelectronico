import numpy as np


def normalizar_array(func):
    def inner(x):
        xmax = np.max(x)
        xmin = np.min(x)

        return func(x, xmax, xmin)

    return inner

@normalizar_array
def normalizar(x, xmax, xmin):
    return 2*(x - xmin)/(xmax - xmin) - 1


def ejemplo_con_decorador():
    print('Ejemplo con decoradores')

    x = np.array([x for x in range(1, 11)])

    xn = normalizar(x)

    for i in range(len(x)):
        print(f'{x[i]:5.2f}  {xn[i]:5.2f}')


def run():
    ejemplo_con_decorador()


if __name__ == '__main__':
    run()
