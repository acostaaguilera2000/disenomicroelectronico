A = set([1, 2, 3])
A_tipo = type(A)


B = {3, 4, 5}
B_tipo = type(B)


print(f'\nA es de tipo: {A_tipo}')
print(A)


print(f'\nB es de tipo: {B_tipo}')
print(B)


print('\n\nOperaciones:\n')
print('A unión B:')
print(A.union(B))

print('\nA intersección B:')
print(A.intersection(B))

print('\nA diferencia B:')
print(A.difference(B))

print('\nB diferencia A:')
print(B.difference(A))

print('\nPreguntar si un elemento está dentro del conjunto:')
print('1 in A:', 1 in A)
print('5 in A:', 5 in A)
print('5 not in A:', 5 not in A)
