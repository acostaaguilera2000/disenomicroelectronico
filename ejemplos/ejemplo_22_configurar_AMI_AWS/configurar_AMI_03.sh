#!/bin/bash

# * ------------------------------------------------------------------------------------
# * "THE BEER-WARE LICENSE" (Revision 42):
# * <diegorestrepoleal@gmail.com> wrote this file. As long as you retain this notice you
# * can do whatever you want with this stuff. If we meet some day, and you think
# * this stuff is worth it, you can buy me a beer in return Diego Andrés Restrepo Leal.
# * ------------------------------------------------------------------------------------

# Limpiar pantalla
clear

# Color de los mensajes
AZUL=$(tput setaf 6)
VERDE=$(tput setaf 2)
ROJO=$(tput setaf 1)
LILA=$(tput setaf 5)
LIMPIAR=$(tput sgr 0)

echo
echo "$VERDE ============= INICIO ============= $LIMPIAR"
echo

echo
echo "$AZUL Instalar gcc $LIMPIAR"
echo
sudo yum install -y gcc

echo
echo "$AZUL Instalar httpd-devel $LIMPIAR"
echo
sudo yum install -y httpd-devel

echo
echo "$AZUL Instalar mod_wsgi $LIMPIAR"
echo
sudo yum install -y mod_wsgi

echo
echo "$AZUL Instalar tmux $LIMPIAR"
echo
sudo yum install -y tmux

echo
echo "$AZUL Instalar paquetes de python $LIMPIAR"
echo
sudo yum install -y python3-devel
sudo pip3 install flask
sudo pip3 install flask-mysql
sudo pip3 install mysql-connector-python
sudo pip3 install python-decouple
sudo pip3 install mod_wsgi

echo
echo "$VERDE FIN $LIMPIAR"
echo

exit 0
