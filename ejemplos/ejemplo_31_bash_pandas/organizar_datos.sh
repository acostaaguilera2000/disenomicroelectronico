#!/bin/bash


clear


if [ ! -d Data_Set_X ];
then
    echo
    echo "Crear directorio Data_Set_X"
    echo
    mkdir Data_Set_X
else
    echo
    echo "Ya existe el directorio"
    echo
fi


if [ ! -d Data_Set_Y ];
then
    echo
    echo "Crear directorio Data_Set_Y"
    echo
    mkdir Data_Set_Y
else
    echo
    echo "Ya existe el directorio"
    echo
fi


echo
echo "Mover archivos a Data_Set_X"
echo
mv DataSet_X* Data_Set_X/

echo
echo "Mover archivos a Data_Set_Y"
echo
mv DataSet_Y* Data_Set_Y/


exit 0
