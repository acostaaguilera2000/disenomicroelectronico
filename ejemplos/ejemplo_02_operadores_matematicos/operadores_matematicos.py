num1 = 5
num2 = 2

palabra1 = 'Hola'
palabra2 = 'Mundo'

result = num1 + num2
print("La suma de {num1} y {num2} es {result}.\n".format(
    num1=num1, num2=num2, result=result))  # Se puede usar: format


result = num1 - num2
# Se puede usar: f-strings
print(f"La resta de {num1} y {num2} es {result}.\n")


result = num1*num2
print(f"La multiplicación de {num1} y {num2} es {result}.\n")


result = num1/num2
print(f"La división de {num1} y {num2} es {result}.\n")


result = num1//num2
print(f"La división entera de {num1} y {num2} es {result}.\n")


result = num1 % num2
print(f"El módulo de {num1} y {num2} es {result}.\n")


result = num1**num2
print(f"{num1} elevado a {num2} es {result}.\n")


result = num1 > num2
print(f"¿{num1} es mayor que {num2}?: {result}.\n")


result = num1 < num2
print(f"¿{num1} es menor que {num2}?: {result}.\n")


result = num1 == num2
print(f"¿{num1} es igual a {num2}?: {result}.\n")


result = palabra1 + palabra2
print(f"La suma de {palabra1} y {palabra2} es {result}.\n")


result = palabra1*num1
print(f"El producto de {palabra1} y {num1} es {result}.\n")
