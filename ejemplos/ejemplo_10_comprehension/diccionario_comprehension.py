def cubos_for():

    cubos = {}

    for num in range(11):
        cubos[chr(num + 65)] = num**3

    print(cubos)


def cubos_diccionario():
    cubos = {chr(num + 65): num**3 for num in range(11)}

    print(cubos)


def run():
    print('\nCubos de los números de 0 a 10, obtenidos con un for: ')
    cubos_for()

    print('\nCubos de los números de 0 a 10, obtenidos con diccionary comprenhension: ')
    cubos_diccionario()


if __name__ == '__main__':
    run()
