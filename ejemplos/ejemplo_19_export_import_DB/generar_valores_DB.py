from numpy import random
from decouple import config
import mysql.connector


SIZE = 50


def conectarDB():
    mydb = mysql.connector.connect(
        host=config('HOST_DB'),
        user=config('USER_DB'),
        password=config('PASSWORD_DB'),
        database=config('DATABASE')
    )

    return mydb


def generar_numeros():
    num = [num for num in range(SIZE)]
    x = random.rand(SIZE)
    y = random.rand(SIZE)

    return num, x, y


def guardarDB(mydb, num, x, y):
    cur = mydb.cursor()

    cur.execute('DELETE FROM numeros')

    for i in range(SIZE):
        cur.execute(
            f'INSERT INTO numeros (num, x, y) VALUES ({num[i]}, {x[i]}, {y[i]})')

    cur.close()


def run():
    mydb = conectarDB()
    num, x, y = generar_numeros()
    guardarDB(mydb, num, x, y)
    mydb.close()


if __name__ == '__main__':
    run()
