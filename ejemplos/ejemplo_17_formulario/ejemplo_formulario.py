from flask import Flask, render_template, request, redirect, url_for, flash


app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/ejemplo1', methods=['POST'])
def ejemplo1():
    if request.method == 'POST':
        nombre = request.form['nombre']
        correo = request.form['correo']
        mensaje = request.form['mensaje']

        print(nombre)
        print(correo)
        print(mensaje)

    return nombre + ' ' + correo + ' ' + mensaje


@app.route('/ejemplo2')
def ejemplo2():
    return render_template('formulario.html')


@app.route('/ejemplo2_visualizar', methods=['POST'])
def ejemplo2_visualizar():
    if request.method == 'POST':
        nombre = request.form['nombre']
        correo = request.form['correo']
        mensaje = request.form['mensaje']

        return render_template('visualizar.html', nombre=nombre, correo=correo, mensaje=mensaje)


@app.route('/ejemplo3', methods=['GET', 'POST'])
def ejemplo3():
    if request.method == 'POST':
        nombre = request.form['nombre']
        correo = request.form['correo']
        mensaje = request.form['mensaje']

        return render_template('visualizar.html', nombre=nombre, correo=correo, mensaje=mensaje)

    return render_template('formulario2.html')


if __name__ == '__main__':
    app.run(debug=True)
