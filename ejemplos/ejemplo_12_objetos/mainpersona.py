from persona import Persona


def run():
    persona0 = Persona(nombre="María", edad=25)
    persona0.presentarse()

    persona1 = Persona(nombre=None, edad=None)
    persona1.nombre = "Juan"
    persona1.edad = 11
    persona1.presentarse()


if __name__ == '__main__':
    run()
