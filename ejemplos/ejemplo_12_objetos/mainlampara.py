from lamp import Lamp


def run():
    lamp = Lamp(is_turned_on=False)

    while True:
        command = str(input('''
			Elija una acción:
			[p]render
			[a]pagar
			[s]salir
			'''))

        if command == 'p':
            lamp.turned_on()
        elif command == 'a':
            lamp.turned_off()
        else:
            break


if __name__ == '__main__':
    run()
