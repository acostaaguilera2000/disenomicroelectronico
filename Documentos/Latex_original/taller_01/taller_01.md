::: titlepage
::: center
**UNIVERSIDAD DEL MAGDALENA**\
**Diseño Microelectrónico digital**\
**Programa de Ingeniería Electrónica**\
Taller 01\
**Python Y SQL**\
:::
:::

# Actividad []{#actividad label="actividad"} {#actividad .unnumbered}

## Primer momento {#primer-momento .unnumbered}

De forma individual, pero colaborativa, una vez concluidos se deben
enviar los códigos por correo electrónico en un archivo en formato ZIP
(Taller_01_Nombre_Apellido_código) al finalizar la clase, el asunto debe
ser:\
DM_Taller_01, con el nombre y el código de la persona que lo resolvió en
el cuerpo del correo.\
**Nota:** Si el tiempo de la clase no alcanza para resolver los
ejercicios, por favor, envíe los que alcanzó a solucionar. Una semana
después, remita todos los códigos.

## Ejercicio 1 {#ejercicio-1 .unnumbered}

Escriba un programa que solicite dos números, a y b, al usuario. El
programa debe calcular:

1.  La suma de a y b.

2.  La diferencia cuando b se resta de a.

3.  El producto entre a y b.

4.  El cociente cuando a es dividido por b.

5.  El resultado de $\log_{10}(a)$.

6.  El resultado de $a^{b}$.

7.  El resultado de $e^{a}$

8.  El resultado de $\sqrt[b]{a}$

## Ejercicio 2 {#ejercicio-2 .unnumbered}

Escriba un programa que solicite cuatro números, muestre la suma de
estos incluyendo los números ingresados:\
Ejemplo: (1.5) + (-4) + (65.34) + (9) = 71.84.

## Ejercicio 3 {#ejercicio-3 .unnumbered}

Un triángulo se puede clasificar en: equilátero, isósceles o escaleno.
Escriba un programa que solicite los valores de los ángulos de un
triángulo y muestre un mensaje en donde indique qué tipo de triángulo
es.

## Ejercicio 4 {#ejercicio-4 .unnumbered}

Escriba un programa que solicite los valores de dos matrices de m filas
y n columnas, calcule:

-   La suma de las dos matrices.

-   El producto de las dos matrices.

-   Calcule el determinante de las dos matrices.

-   Calcule la inversa de la dos matrices.

**Nota:** Tenga en cuenta todos los conceptos del álgebra lineal para
realizar las operaciones. Explore la librería
[numpy](https://numpy.org/) para resolver este ejercicio.

## Ejercicio 5 {#ejercicio-5 .unnumbered}

Escriba un programa que solicite el mes, representado por enteros
positivos desde 1 hasta 12 y el año, representado por una cifra positiva
de cuatro números. El programa debe imprimir el nombre del mes y la
cantidad de días que este posee, debe tener en cuenta si un año es
bisiesto o no para mostrar los días correctos del mes de febrero.

## [Importante:]{style="color: red"} {#importante .unnumbered}

Para resolver los siguientes ejercicios debe crearse una cuenta en la
plataforma
[[Codingame](https://www.codingame.com/start)]{style="color: blue"}

## Ejercicio 6 {#ejercicio-6 .unnumbered}

[[ONBOARDING](https://www.codingame.com/training/easy/onboarding)]{style="color: blue"}

## Ejercicio 7 {#ejercicio-7 .unnumbered}

[[TEMPERATURES](https://www.codingame.com/training/easy/temperatures)]{style="color: blue"}

## Ejercicio 8 {#ejercicio-8 .unnumbered}

[[HORSE-RACING
DUALS](https://www.codingame.com/training/easy/horse-racing-duals)]{style="color: blue"}

## Ejercicio 9 {#ejercicio-9 .unnumbered}

[[CREDIT CARD VERIFIER (LUHN'S
ALGORITHM)](https://www.codingame.com/training/easy/credit-card-verifier-luhns-algorithm)]{style="color: blue"}

## Ejercicio 10 {#ejercicio-10 .unnumbered}

[[SUBSTITUTION
ENCODING](https://www.codingame.com/training/easy/substitution-encoding)]{style="color: blue"}

## [Importante:]{style="color: red"} {#importante-1 .unnumbered}

Para resolver los siguientes ejercicios debe crearse una cuenta en la
plataforma
[[HackerRank](https://www.hackerrank.com)]{style="color: blue"}.\
**Nota:** Elegir MySQL en el IDE de la plataforma.

## Ejercicio 11 {#ejercicio-11 .unnumbered}

[[SQL
Tutorial](https://www.w3schools.com/sql/default.asp)]{style="color: blue"}

## Ejercicio 12 {#ejercicio-12 .unnumbered}

[[Revising the Select
Query](https://www.hackerrank.com/challenges/revising-the-select-query/problem?isFullScreen=true)]{style="color: blue"}

## Ejercicio 13 {#ejercicio-13 .unnumbered}

[[Revising the Select Query
II](https://www.hackerrank.com/challenges/revising-the-select-query-2/problem?isFullScreen=true)]{style="color: blue"}

## Ejercicio 14 {#ejercicio-14 .unnumbered}

[[Select
All](https://www.hackerrank.com/challenges/select-all-sql/problem?isFullScreen=true)]{style="color: blue"}

## Ejercicio 15 {#ejercicio-15 .unnumbered}

[[Select by
ID](https://www.hackerrank.com/challenges/select-by-id/problem?isFullScreen=true)]{style="color: blue"}

## Ejercicio 16 {#ejercicio-16 .unnumbered}

[[Japanese Cities'
Attributes](https://www.hackerrank.com/challenges/japanese-cities-attributes/problem?isFullScreen=true)]{style="color: blue"}

## Ejercicio 17 {#ejercicio-17 .unnumbered}

[[Japanese Cities'
Names](https://www.hackerrank.com/challenges/japanese-cities-name/problem?isFullScreen=true)]{style="color: blue"}

## Ejercicio 18 {#ejercicio-18 .unnumbered}

[[Weather Observation Station
1](https://www.hackerrank.com/challenges/weather-observation-station-1/problem?isFullScreen=true)]{style="color: blue"}

## Ejercicio 19 {#ejercicio-19 .unnumbered}

[[Weather Observation Station
2](https://www.hackerrank.com/challenges/weather-observation-station-2/problem?isFullScreen=true)]{style="color: blue"}

## Ejercicio 20 {#ejercicio-20 .unnumbered}

[[Weather Observation Station
3](https://www.hackerrank.com/challenges/weather-observation-station-3/problem?isFullScreen=true)]{style="color: blue"}

## Ejercicio 21 {#ejercicio-21 .unnumbered}

[[Weather Observation Station
4](https://www.hackerrank.com/challenges/weather-observation-station-4/problem?isFullScreen=true)]{style="color: blue"}
