# Laboratorio portátil 

<br>

Para desarrollar diferentes proyectos y prototipos en la ingeniería electrónica se requiere un conjunto de herramientas de medición:
- Multímetro.
- Osciloscopio.
- Fuente de alimentación variable.
- Generador de señales.

Cada equipo se encuentra alojado en un banco de trabajo del laboratorio, sin embargo, hay soluciones portables que integran todos estos equipos, una ampliamente conocida es el [NI ELVIS](https://www.ni.com/es-co/support/model.ni-elvis-iii.html), el cual en su modelo III permite mostrar por medio de una interface web las señales que se están midiendo en tiempo real.

<br>

Desarrolle un prototipo de laboratorio portátil, que implemente una aplicación web como pantalla para mostrar los parámetros en tiempo real, con las siguientes características.
- **Fuente de voltaje DC variable:** Dos (02) canales de 0v a 10v. 
- **Osciloscopio:** Dos (02) canales.
    - **Nota:** La frecuencia de muestreo debe ser determinada según el criterio del equipo de trabajo.
- **Multímetro:** 
    - Debe medir continuidad.
    - Voltaje.
    - Corriente.
    - **Nota:** Los valores máximos deben ser definidos por el equipo de trabajo, sin embargo, el valor de voltaje máximo que se puede medir no debe ser inferior a 10 v y la corriente no debe ser inferior a 250 mA.
- **Generador de señales:** La forma y la frecuencia de la señal debe ser determinado por el equipo de trabajo.

<br>

**Importante:** El prototipo debe ser presentado en PCB diseñada por el equipo de trabajo y contar con un chasis (carcaza) resistente.



